# Schemic

A tool for managing Postgres migrations using only SQL.

## Installing Schemic

Run the following command to install the latest version of schemic locally. 
You can run the same command to update to the latest version.

```sh
curl https://gitlab.com/elibdev/schemic/raw/main/schemic > /usr/local/bin/schemic; chmod +x /usr/local/bin/schemic
```

## Initial Ideas

- write it as a shell script
- represent migrations in SQL files
- use only roll forward approach (down migrations not needed)
- apply unapplied migrations
- store migrations in a database table named `schemic` with the time it ran
- enable testing application code across database schema from before and after latest migration
- dump schema (`pg_dump --schema-only --no-privileges --no-owner [dbname] > schema.sql`) on migration

## CLI

- `schemic init` - create migrations folder with an initial migration to create `schemic` table
- `schemic migrate [database-name]` - run all unapplied migrations on the database
- `schemic generate [migration-name]` - create new empty database and initialize migrations for them

```sql
CREATE TABLE schemic (
    id TEXT PRIMARY KEY,
    ran_at TIMESTAMP NOT NULL
)
```

```sh
echo "$(date +"%Y_%m_%d_%H_%M_%S")__initial_migration.sql"
psql schemic_test -c "select * from schema_migrations" --csv
```

## Development Setup

- `brew install shellcheck` to install a shell linter
- `shellcheck schemic` to run shellcheck on the source